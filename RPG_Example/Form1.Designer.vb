﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.infoStrip = New System.Windows.Forms.StatusStrip()
        Me.infoStripMapTile = New System.Windows.Forms.ToolStripStatusLabel()
        Me.infoStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'infoStrip
        '
        Me.infoStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.infoStripMapTile})
        Me.infoStrip.Location = New System.Drawing.Point(0, 369)
        Me.infoStrip.Name = "infoStrip"
        Me.infoStrip.Size = New System.Drawing.Size(535, 22)
        Me.infoStrip.TabIndex = 0
        Me.infoStrip.Text = "StatusStrip1"
        '
        'infoStripMapTile
        '
        Me.infoStripMapTile.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.infoStripMapTile.Name = "infoStripMapTile"
        Me.infoStripMapTile.Size = New System.Drawing.Size(0, 17)
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(535, 391)
        Me.Controls.Add(Me.infoStrip)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.infoStrip.ResumeLayout(False)
        Me.infoStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents infoStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents infoStripMapTile As System.Windows.Forms.ToolStripStatusLabel

End Class
