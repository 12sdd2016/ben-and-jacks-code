﻿Imports System.IO


Public Class Form1

    Const gridX As Integer = 9
    Const gridY As Integer = 9

    Dim filepath As String = Application.StartupPath & "\"
    Dim mapgrid(gridX, gridY) As PictureBox
    Dim mapStuff(gridX, gridY) As map
    Dim dudeX As Integer = 2
    Dim dudeY As Integer = 2
    Dim dudeColor As Color = Color.White

    Structure map
        Dim x As Integer
        Dim y As Integer
        Dim terrain As String
        Dim canpass As Boolean
        Dim item As String
    End Structure

    Private Sub Form1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress

        mapgrid(dudeX, dudeY).BackColor = getMapColor(mapStuff(dudeX, dudeY).terrain)

        Select Case e.KeyChar
            Case "w"
                If dudeY > 0 Then
                    If mapStuff(dudeX, dudeY - 1).canpass = True Then
                        dudeY -= 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Top += 70
                            Next
                        Next
                    End If
                End If
                
            Case "s"
                If dudeY < gridY Then
                    If mapStuff(dudeX, dudeY + 1).canpass = True Then
                        dudeY += 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Top -= 70
                            Next
                        Next
                    End If
                End If
            Case "a"
                If dudeX > 0 Then
                    If mapStuff(dudeX - 1, dudeY).canpass = True Then
                        dudeX -= 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Left += 70
                            Next
                        Next
                    End If
                End If
            Case "d"
                If dudeX < gridX Then
                    If mapStuff(dudeX + 1, dudeY).canpass = True Then
                        dudeX += 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Left -= 70
                            Next
                        Next
                    End If
                End If
            Case "i"
                ' Opens inventory
        End Select

        mapgrid(dudeX, dudeY).BackColor = dudeColor
        infoStripMapTile.Text = mapStuff(dudeX, dudeY).terrain

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        createMap()
        load_map()
        draw_map()

        mapgrid(dudeX, dudeY).BackColor = dudeColor

    End Sub

    Private Sub draw_map()

        For x = 0 To gridX
            For y = 0 To gridY
                mapgrid(x, y).BackColor = getMapColor(mapStuff(x, y).terrain)
            Next
        Next

    End Sub

    Private Function getMapColor(mapTerrain As String) As Color
        Select Case mapTerrain
            Case "lava"
                Return Color.Red
            Case "grass"
                Return Color.LawnGreen
            Case "water"
                Return Color.Aquamarine
            Case "mountain"
                Return Color.DarkSlateGray
        End Select
    End Function

    Private Sub createMap()

        For x = 0 To gridX
            For y = 0 To gridY
                Dim pic As New PictureBox
                pic.Left = 70 * x + 10
                pic.Top = 70 * y + 10
                pic.Height = 69
                pic.Width = 69
                mapgrid(x, y) = pic
                pic.BackColor = Color.Plum
                Me.Controls.Add(pic)
            Next
        Next
    End Sub

    Private Sub load_map()

        'Dim oFile As System.IO.File
        Dim objReader As New System.IO.StreamReader(filepath & "maptest.txt")
        Dim lineIn As String
        Dim tmp

        ' Read in each line from the file
        Do While objReader.Peek() <> -1     ' checks to see if the file has more lines 
            lineIn = objReader.ReadLine()   ' reads a line into a string
            tmp = Split(lineIn, "|")        ' splits the text into fields End While
            mapStuff(Int(tmp(0)), Int(tmp(1))).x = tmp(0)
            mapStuff(Int(tmp(0)), Int(tmp(1))).y = tmp(1)
            mapStuff(Int(tmp(0)), Int(tmp(1))).terrain = tmp(2)
            mapStuff(Int(tmp(0)), Int(tmp(1))).canpass = CBool(tmp(3))
            mapStuff(Int(tmp(0)), Int(tmp(1))).item = tmp(4)
        Loop

        objReader.Close()

    End Sub
End Class
